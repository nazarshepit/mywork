const firstA = document.getElementsByClassName("headerA");
for(let elem of firstA){
    elem.onmouseover=function(){
        this.style.color="#1E1E1E";
	    this.style.background="white";
	}
    elem.onmouseleave=function(){
        this.style.color="white";
	    this.style.background="#1E1E1E";
	}
}
const buttons= document.querySelectorAll('input[type="button"]');
for(let elem of buttons){
	elem.onmouseover = function(){
        this.style.color="#1E1E1E";
	    this.style.background="white";
	}
	elem.onmouseleave=function(){
        this.style.color="white";
	    this.style.background="#1E1E1E";
	}
	elem.onmousedown = function(){
        this.style.color="#1E1E1E";
	    this.style.background="grey";
	}
	elem.onmouseup=function(){
        this.style.color="#1E1E1E";
	    this.style.background="white";
	}
}
