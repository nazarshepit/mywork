const name=document.querySelector('input[name="name"]');
const description=document.querySelector('input[name="description"]');
const date=document.querySelector("input[type='date']");
const btnSave=document.querySelector("#save");
const btnDelete=document.querySelector("#delete");
const btnReset=document.querySelector("#reset");
const btnEdit=document.querySelector("#edit");
const listUl=document.querySelector('ul');
const form=document.querySelector('form');
const testDone=document.querySelector("#isDoneYes");
const testNotDone=document.querySelector("#isDoneNo");
let index;
let listTests=[];


class Test{
	#_name;
	#_description;
	#_date;
	#_index;
	#_isDone;
	constructor(name,description,date,isDone){
		this.#_name=name;
		this.#_description=description;
		this.#_date=new Date(date);
		this.#_isDone=isDone;
	}
	set setIndex(index){this.#_index=index;}
	get getAll () {return [this.#_name,this.#_description,this.#_date,this.#_index,this.#_isDone];}
	print(){
		console.log ('name: '+ this.#_name+" Description: "+ this.#_description +
			" Date: "+ `${this.#_date.getDate()}.${this.#_date.getMonth()+1}.
			${this.#_date.getFullYear()}`);
	}
	toJSON(){return`${this.#_name}_${this.#_description}_${this.#_date}_${this.#_index}_${this.#_isDone}`;}
	printTest(){
		name.value=this.#_name;
		description.value=this.#_description;
		date.valueAsDate=this.#_date;
		if (this.#_isDone==true){testDone.checked=true;}
		else{testNotDone.checked=true;}
	}
}


function addTestBtn (event){
	event.preventDefault();
	addTest();	
}

function editTestBtn(event){
	event.preventDefault();
	deleteTest();
	addTest();
}

function deleteTestBtn(event){
	event.preventDefault();
	deleteTest();
}

function resetInputBtn(event){
	event.preventDefault();
	resetInput();
}

function addTest (){
	resetListStyle();
	listUl.appendChild(createLiElement());
	resetInput();
	console.log(listTests);
}

function resetInput(){
	name.value="";
	description.value="";
	date.value="";
	testDone.checked=false;
	testNotDone.checked=false;
	resetListStyle();	
}

function deleteTest(){
	const liList=document.getElementsByTagName("li");
	for (let i=0;i<liList.length;i++){
		if(liList[i].className=="focused"){
		listTests.splice(parseInt(liList[i].id),1);}
		console.log(liList[i].id);
		listUl.removeChild(elem); 
		return;
	}	
    console.log(listTests);
}

function resetListStyle(){
	const liList=document.getElementsByTagName("li");
	for (elem of liList){elem.className="";}
}

function createLiElement(){
	let checked=false;
	if (testDone.checked==true){checked=true;}
	const test=new Test(name.value,description.value,Date.now(),checked);
	test.setIndex=listTests.length;
	listTests.push(test);
	const li = document.createElement('li');
	li.innerHTML=name.value;
	li.id=listTests.length-1;
	li.addEventListener('click',function(){
		resetListStyle();
		li.classList.add('focused');
		index=listTests.length-1;
		listTests[parseInt(li.id)].printTest();
	});
	return li;
}

function createLiElementFromJson(elem,list){
	t = new Test(elem[0],elem[1],elem[2],elem[4]);
	t.setIndex=parseInt(elem[3]);
    list.push(t);
	const li = document.createElement('li');
	li.innerHTML=elem[0];
	li.id=elem[3];
	listUl.appendChild(li);
	li.addEventListener('click',function(){
		resetListStyle();
		li.classList.add('focused');
		index=listTests.length-1;
		listTests[parseInt(li.id)].printTest();
	});
}


	// const newTest=new Test("first","fDescription",Date.now(),true);
	// newTest.setIndex=0;
	// listTests.push(newTest);
	// const newTest1=new Test("second","sDescription",Date.now(),true);
	// newTest1.setIndex=1;
	// listTests.push(newTest1);
	// const newTest2=new Test("third","thDescription",Date.now(),true);
	// newTest2.setIndex=2;
	// listTests.push(newTest2);


function setToStorage(){
	for (let i=0;i<listTests.length;i++){
		localStorage.setItem(i,listTests[i].toJSON());
	}
}

function getFromStorage(list){
	let elem;
	let t;
    for (let i=0;i<localStorage.length;i++){
		elem=(localStorage.getItem(i));
		console.log(elem);
		elem=elem.split('_');
		createLiElementFromJson(elem,list);
	}
}
btnSave.addEventListener("click",addTestBtn);
btnEdit.addEventListener("click",editTestBtn);
btnDelete.addEventListener("click",deleteTestBtn);
btnReset.addEventListener("click",resetInputBtn);
window.addEventListener('beforeunload',setToStorage);

getFromStorage(listTests);

console.log(listTests);